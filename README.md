# Nemuru API Docs

Using [Redocly/redoc](https://github.com/Redocly/redoc), which supports OpenAPI v3.0.

Our API specification is written in YAML and lives in the `/docs` directory, and each new version of the API should be included in a subdirectory following the `v[x]` naming convention. This subdirectory must include:

- `docs/v2/nemuru.yaml`: API specification
- `docs/v2/favicon.png`: Favicon

## To run

```shell
docker build -t nemuru-redoc .
docker run -it --rm -p 80:80 nemuru-redoc
```

[See passing options](https://github.com/Redocly/redoc#redoc-options-object)

```shell
docker run -it --rm -p 80:80 \
    -e REDOC_OPTIONS="native-scrollbars=true" \
    -e REDOC_OPTIONS="path-in-middle-panel" \
    nemuru-redoc
```

Or changing the Dockerfile env variable.
